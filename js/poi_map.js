var poi_map = (function() {
	
  var gMap;
  var gSvp;
  var gSvpClient;
  var gPl;

  var gPathsVisible = false;
  var gPanoramioLayerVisible = true;
  var gFlickrLayerVisible = true;

  var gLatLng;
  var gMarker;

	var gBaseUrl;

  return {
    // a blank object to hold photo ids having markers on the map
    markersInMap : {},

    // an array containing all Flickr markers
    flickrMarkers : new Array(),

    updateStatus : function() {

//			jQuery("#mapinfo").html(gMap.getBounds().toString());

      jQuery("#flickr-search-form input[name=lat0]").attr("value", gMap.getBounds().getSouthWest().lat());
      jQuery("#flickr-search-form input[name=lon0]").attr("value", gMap.getBounds().getSouthWest().lng());
      jQuery("#flickr-search-form input[name=lat1]").attr("value", gMap.getBounds().getNorthEast().lat());
      jQuery("#flickr-search-form input[name=lon1]").attr("value", gMap.getBounds().getNorthEast().lng());

    },

    genPhotoLink : function(photo, lSize, where) {
      var str = "";
      if(!where) {
        var tn_url = "http://farm" + photo.farm + ".static.flickr.com/" + photo.server + "/" + photo.id + "_" + photo.secret + "_" + "t.jpg";
        var p_url = "http://www.flickr.com/photos/" + photo.owner + "/" + photo.id;
        str = "<a href=\"" + p_url + "\">" + "<img alt=\""+ photo.title + "\"src=\"" + tn_url + "\"/>" + "</a>";
      }
      else {
        var imageUrl = "http://farm" + photo.farm + ".static.flickr.com/" + photo.server + "/" + photo.id + "_" + photo.secret + "_" + "s.jpg";
        var imageLargeUrl = "http://farm" + photo.farm + ".static.flickr.com/" + photo.server + "/" + photo.id + "_" + photo.secret + "_" + lSize + ".jpg";
        str = "<a href=\"" + imageLargeUrl + "\"><img src=\"" + imageUrl + "\" alt=\"" + photo.title + "\"  /></a>";
      }
      return str;
    },

    genPaging : function(rsp) {
      var hits = rsp.photos.total;
      var page = rsp.photos.page;
      var pages = rsp.photos.pages;

      var nextPage = page;
      var prevPage = page;

      prev_string = "<a class=\"prev_page_class\" href=\"#\"> << Previous page </a>";
      next_string = "<a class=\"next_page_class\" href=\"#\"> Next page >> </a>";

      if(page == 1){
				// show only first page
        if(pages == 1) {
          prev_string = "";
          next_string = "";
        }
				// show first page and next page link
        else {
          prev_string = "";
          nextPage = page + 1;
        }
      }
      else {
				// show last page and previous page link
        if(page == pages) { 
          prevPage = page - 1;
          next_string = "";
        }
				// show middle page and links to both directions
        else {
          prevPage = page - 1;
          nextPage = page + 1;
        }
      }

      jQuery("div#info").text(hits + " hits, "+ pages + " pages");
      jQuery("div#info").append("<br />"+ prev_string +" Page "+ page + next_string);

      jQuery(".prev_page_class").click(function() {
        jQuery("#flickr-search-form input[name=page]").attr("value", prevPage);
        jQuery.post(gBaseUrl + "/poi_search", jQuery("#flickr-search-form").serialize(), poi_map.showImages, "json");
      });

      jQuery(".next_page_class").click(function() {
        jQuery("#flickr-search-form input[name=page]").attr("value", nextPage);
        jQuery.post(gBaseUrl + "/poi_search", jQuery("#flickr-search-form").serialize(), poi_map.showImages, "json");
      });
    },

    createMarker : function(point, msg, opt) {
      var flickr_marker = new GMarker(point,opt);
      GEvent.addListener(flickr_marker, "click", function() {
        flickr_marker.openInfoWindowHtml(msg);
      });
      return flickr_marker;
    },

    showImages : function(response) {
      if(response["stat"] == "ok") {
        var photos = response.photos.photo;
				var large_size = jQuery("#flickr-search-form select[name=size]").val();
        var str= "";

        // wrap photos to html elements
        jQuery.each(photos, function(index,photo) {
          str += poi_map.genPhotoLink(photo, large_size, "toList");

          // create new photo marker to map
          if (!(photo.id in poi_map.markersInMap)) {
            var point = new GLatLng (photo.latitude, photo.longitude);
            var myIcon = new GIcon(G_DEFAULT_ICON);
            myIcon.image = "http://farm" + photo.farm + ".static.flickr.com/" + photo.server + "/" + photo.id + "_" + photo.secret + "_" + "t.jpg";
            myIcon.iconSize = new GSize(32, 32);
            var markerOptions = { icon:myIcon };

            var msg = photo.title + "<br>" + poi_map.genPhotoLink(photo);
            poi_map.flickrMarkers.push(poi_map.createMarker(point, msg, markerOptions));
            //poi_map.createMarker(point, msg, markerOptions);

            poi_map.markersInMap[photo.id] = "";  // store nothing so far
          }

        });

        jQuery.each(poi_map.flickrMarkers, function(index,marker) {
          gMap.addOverlay(marker);
        });

        jQuery("#results").html(str);
			
        poi_map.genPaging(response);
      }
      else {
        jQuery("#results").html("<div class=\"error\">an error occured</div>");
      }
    },

    onMapMove : function() {
      poi_map.updateStatus();
    },

    onMapZoom : function(oldZoom, newZoom) {
      poi_map.updateStatus();
    },

    initialize : function() {
			
			gBaseUrl = Drupal.settings.poi.base_url;
			var mapWidth = Drupal.settings.poi.map_width;
			var mapHeight = Drupal.settings.poi.map_height;
	
      jQuery("#gmap_stuff").append("<div id=\"map_canvas\" style=\"width:"+ mapWidth +"; height:"+ mapHeight +"; margin: 10px 5px\"></div>");
      jQuery("#gmap_stuff").append("<input type=\"button\" onclick=\"poi_map.showAvailablePaths()\" value=\"Toggle paths\"/>");
      jQuery("#gmap_stuff").append("<input type=\"button\" onclick=\"poi_map.showPanoramioLayer()\" value=\"Toggle Panoramio\"/>");
      jQuery("#gmap_stuff").append("<input type=\"button\" onclick=\"poi_map.showFlickrLayer()\" value=\"Toggle Flickr\"/>");
      jQuery("#gmap_stuff").append("<div name=\"pano\" id=\"pano\" style=\"width:"+ mapWidth +"; height:"+ mapHeight +"; margin: 10px 5px\"></div>");

    	var lat = jQuery(".latitude").attr("title");
    	var lng = jQuery(".longitude").attr("title");

    	gLatLng = new GLatLng(lat,lng);
    	gMarker = new GMarker(gLatLng);
			
      poi_map.createNewMap();
      poi_map.addMapEventListener();

      poi_map.createNewSVPanorama();
      poi_map.addSVEventListener();

      poi_map.updateStatus();
    },

    createNewMap : function() {
      gPl = new GLayer("com.panoramio.all");
      gMap = new GMap2(document.getElementById("map_canvas"));
      gMap.setCenter(gLatLng, 15);
      gMap.setUIToDefault();
      gMap.addOverlay(gPl);
			gMap.addOverlay(gMarker);
    },

    addMapEventListener : function() {
      GEvent.addListener(gMap,"moveend",this.onMapMove);
      GEvent.addListener(gMap,"zoomend",this.onMapZoom);

			// A GStreetviewClient object performs searches for Street View data based on parameters that are passed to its methods.
      gSvpClient = poi_map.newSVClient();

      GEvent.addListener(gMap, "click", function(overlay, latlng) {
        if (latlng) {
					gMap.removeOverlay(gMarker);
					gMarker = new GMarker(latlng);
					gMap.addOverlay(gMarker);
					
					// getNearestPanorama(): Retrieves the data for the nearest panorama to a given latlng and passes it to the provided callback as a GStreetviewData object.
          gSvpClient.getNearestPanorama(latlng, poi_map.showPanoData);
          jQuery("#flickr-search-form input[name=page]").attr("value", "1"); // go to first page anyway
          jQuery.post(gBaseUrl + "/poi_search", jQuery("#flickr-search-form").serialize(), poi_map.showImages, "json");
        }
      });
    },

    newSVClient : function() {
      var svc = new GStreetviewClient();
      return svc;
    },

    createNewSVPanorama : function() {
      var myPOV = {yaw:1.0,pitch:-2.0};
      gSvp = new GStreetviewPanorama(document.getElementById("pano"));
      gSvp.setLocationAndPOV(gLatLng, myPOV);
    },

    addSVEventListener : function() {
      // update map marker position to new coordinates
      GEvent.addListener(gSvp, "pitchchanged", function(pitch) {
        if (pitch) {
          latlng = gSvp.getLatLng();
          gMap.removeOverlay(gMarker);
          gMarker = new GMarker(latlng);
          gMap.addOverlay(gMarker);
        }
      });
      GEvent.addListener(gSvp, "error", poi_map.handleNoFlash);
    },

    showPanoData : function(panoData) {
      if (panoData.code != 200) {
				// if no pano data, hide street view
				gSvp.hide();
				return;
      }

      gMap.removeOverlay(gMarker);
      gMarker = new GMarker(panoData.location.latlng);
      gMap.addOverlay(gMarker);
			
      gSvp.setLocationAndPOV(panoData.location.latlng);
    },


    showAvailablePaths : function() {
      if(!gPathsVisible) {
        svOverlay = new GStreetviewOverlay();
        gMap.addOverlay(svOverlay);
        gPathsVisible = true;
      }
      else {
        gMap.removeOverlay(svOverlay);
        gPathsVisible = false;
      }
    },

    showPanoramioLayer : function() {
      if(!gPanoramioLayerVisible) {
        gPl = new GLayer("com.panoramio.all");
        gMap.addOverlay(gPl);
        gPanoramioLayerVisible = true;
      }
      else {
        gMap.removeOverlay(gPl);
        gPanoramioLayerVisible = false;
      }
    },

    showFlickrLayer : function() {
      if(!gFlickrLayerVisible) {
        jQuery.each(poi_map.flickrMarkers, function(index,value) {
          gMap.addOverlay(value);
        });
        gFlickrLayerVisible = true;
      }
      else {
        jQuery.each(poi_map.flickrMarkers, function(index,value) {
          gMap.removeOverlay(value);
        });
        gFlickrLayerVisible = false;
      }
    },

    handleNoFlash : function(errorCode) {
      if (errorCode == 603) {
        alert("Error: no Flash support detected.");
        return;
      }
    }


  }

})();