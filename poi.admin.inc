<?php

/**
 * @file
 * Administration page callbacks for the poi module.
 */

/**
 * Configure poi settings.
 */
function poi_admin_settings() {

  $form = array();
	global $base_url;
	$gmap_path = $base_url."/admin/config/services/gmap";
	$loc_path = $base_url."/admin/config/content/location";
  $api_key = variable_get('poi_flickr_api_key', '');
	
	$form['info'] = array (
      '#markup' => ('
				<h4>Other configuration</h4>
				<div>Remember to set your Google Maps API key at <a href="'.$gmap_path.'">Google Maps settings</a></div>
				<div>and at <a href="'.$loc_path.'">Location</a> settings tick at \'Use a Google Map to set latitude and longitude\'</div>
			'),
	);
	
  $form['poi_flickr_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('Your Flickr API key'),
      '#required' => TRUE,
      '#default_value' => $api_key,
  );
  if (empty($api_key)) {
    $form['poi_flickr_api_key']['#description'] = t('Get Flickr API key. !link', array(
        '!link' => l(t('Read more'), 'http://www.flickr.com/services/api/misc.api_keys.html'))
    );
  }
  else {
    $form['flickrapi_api_key']['#description'] = t('API Key from Flickr.');
  }
	
	$poi_gmap_width = variable_get('poi_gmap_width', '');
	if (empty($poi_gmap_width)) {
		$poi_gmap_width = '480px';
	}
	$form['poi_gmap_width'] = array(
      '#type' => 'select',
      '#title' => t('Map width'),
      '#options' => array(
				'160px' => '160px',
				'320px' => '320px',
				'480px' => '480px',
				'600px' => '600px',
				'720px' => '720px',
				'800px' => '800px',
				'960px' => '960px',
     ),
      '#default_value' => $poi_gmap_width,
  );
	
	$poi_gmap_height = variable_get('poi_gmap_height', '');
	if (empty($poi_gmap_height)) {
		$poi_gmap_height = '320px';
	}
	$form['poi_gmap_height'] = array(
      '#type' => 'select',
      '#title' => t('Map height'),
      '#options' => array(
				'160px' => '160px',
				'240px' => '240px',
				'320px' => '320px',
				'400px' => '400px',
				'480px' => '480px',
     ),
      '#default_value' => $poi_gmap_height,
  );
	
	
  $form['#validate'][] = 'poi_admin_settings_validate';
  return system_settings_form($form);
}




/**
 * Validate admin settings form.
 */
function poi_admin_settings_validate($form, &$form_state) {
  $key = trim($form_state['values']['poi_flickr_api_key']);

  if ($key && (preg_match('/^[A-Fa-f\d]{32}$/', $key) != 1)) {
    form_set_error('poi_flickr_api_key', t('This does not appear to be a Flickr API key.'));
  }
}
